pragma solidity ^0.4.20;

contract Coinflip {
  address public owner;

  uint public constant NUM_SIDES = 2;
  string[NUM_SIDES] public SIDE_NAME = ["heads", "tails"];
  enum SideType { Heads, Tails, None }
  SideType public winningTeam = SideType.None;

  uint public constant BOOKIE_POOL_COMMISSION = 10;
  uint public constant MINIMUM_BET = 0.001 ether;
  uint public constant TOTAL_AMOUNT_NEEDED = 0.5 ether;

  bool public payoutCompleted;

  struct Better {
    uint[NUM_SIDES] amountsBet;
  }

  mapping(address => Better) betterInfo;
  address[] betters;
  uint[NUM_SIDES] public totalAmountsBet;
  uint[NUM_SIDES] public totalUsersJoined;

  uint[] public previousWinningTeams;
  uint[] public previousPayouts;

  uint public numberOfBets;
  uint public totalBetAmount;

  /* Events */

  event BetMade();
  event TotalAmountReached();
  event OwnerGetTenPercent(uint _amount);
  event TransferEtherToWinner(address _address, uint _amount);

  /* Modifiers */

  modifier canPerformPayout() {
    if (winningTeam != SideType.None && !payoutCompleted && totalBetAmount >= TOTAL_AMOUNT_NEEDED) _;
  }

  modifier bettingIsClosed() {
    if (totalBetAmount >= TOTAL_AMOUNT_NEEDED) _;
  }

  modifier onlyBookieLevel() {
    require(owner == msg.sender); _;
  }

  // Constructor
  function Coinflip() public {
    owner = msg.sender;
  }

  function () public payable {}

  function getUserBets() public constant returns(uint[NUM_SIDES]) {
    return betterInfo[msg.sender].amountsBet;
  }

  function canBet() public constant returns(bool) {
    return (totalBetAmount < TOTAL_AMOUNT_NEEDED);
  }

  function bet(uint teamIdx) public payable {
    require(canBet() == true);
    require(SideType(teamIdx) == SideType.Heads || SideType(teamIdx) == SideType.Tails);
    require(msg.value >= MINIMUM_BET);

    if (betterInfo[msg.sender].amountsBet[0] == 0 && betterInfo[msg.sender].amountsBet[1] == 0)
    betters.push(msg.sender);

    // Perform bet
    betterInfo[msg.sender].amountsBet[teamIdx] += msg.value;
    numberOfBets++;
    totalBetAmount += msg.value;
    totalAmountsBet[teamIdx] += msg.value;
    totalUsersJoined[teamIdx] += 1;

    BetMade();

    if (totalBetAmount >= TOTAL_AMOUNT_NEEDED && totalAmountsBet[0] > 0 && totalAmountsBet[1] > 0) {
      TotalAmountReached();
      triggerPayout();
    }
  }

  function multiBlockRandomGen(uint seed, uint size) private view returns (bool randomBoolean) {
    uint n = 0;
    for (uint i = 0; i < size; i++){
      if (uint(sha3(block.blockhash(block.number-i-1), seed ))%2==0)
      n += 2**i;
    }
    return n / 2 == 0;
  }

  function triggerPayout() public onlyBookieLevel {
    bool randomBoolean = multiBlockRandomGen(now, 2);
    if (randomBoolean) {
      winningTeam = SideType(0);
      } else {
        winningTeam = SideType(1);
      }

      performPayout();
    }

  function performPayout() private canPerformPayout {
    uint losingChunk = this.balance - totalAmountsBet[uint(winningTeam)];
    uint bookiePayout = losingChunk / BOOKIE_POOL_COMMISSION + totalUsersJoined[uint(winningTeam)] * 1300000000000000;

    owner.transfer(bookiePayout);
    OwnerGetTenPercent(bookiePayout);

    uint totalPayout = 0;

    for (uint k = 0; k < betters.length; k++) {
      uint betOnWinner = betterInfo[betters[k]].amountsBet[uint(winningTeam)];
      uint payout = betOnWinner + ((betOnWinner * (losingChunk - bookiePayout)) / totalAmountsBet[uint(winningTeam)]);

      if (payout > 0) {
        betters[k].transfer(payout);
        totalPayout += payout;
        TransferEtherToWinner(betters[k], payout);
      }

      betterInfo[betters[k]].amountsBet[0] = 0;
      betterInfo[betters[k]].amountsBet[1] = 0;
    }

    previousWinningTeams.push(uint(winningTeam));
    previousPayouts.push(totalPayout);

    payoutCompleted = false;
    totalBetAmount = 0;
    numberOfBets = 0;
    delete totalAmountsBet;
    delete betters;
    delete totalUsersJoined;
    winningTeam = SideType(2);
  }
}
