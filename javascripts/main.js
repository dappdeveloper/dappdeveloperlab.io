App = {
    web3Provider: null,
    betInstance: null,
    contracts: {},

    initWeb3: function() {
        if (typeof web3 !== 'undefined') {
            App.web3Provider = web3.currentProvider;
        } else {
            App.web3Provider = new Web3.providers.HttpProvider('http://localhost:8545');
        }
        web3 = new Web3(App.web3Provider);
        return App.initContract();
    },
    initContract: function() {
        var NumberBetContract = web3.eth.contract([
            {
                "constant": true,
                "inputs": [],
                "name": "getUserBets",
                "outputs": [
                    {
                        "name": "",
                        "type": "uint256[2]"
                    }
                ],
                "payable": false,
                "stateMutability": "view",
                "type": "function"
            },
            {
                "constant": false,
                "inputs": [
                    {
                        "name": "myid",
                        "type": "bytes32"
                    },
                    {
                        "name": "result",
                        "type": "string"
                    }
                ],
                "name": "__callback",
                "outputs": [],
                "payable": false,
                "stateMutability": "nonpayable",
                "type": "function"
            },
            {
                "constant": true,
                "inputs": [],
                "name": "numberOfBets",
                "outputs": [
                    {
                        "name": "",
                        "type": "uint256"
                    }
                ],
                "payable": false,
                "stateMutability": "view",
                "type": "function"
            },
            {
                "constant": true,
                "inputs": [],
                "name": "NUM_TEAMS",
                "outputs": [
                    {
                        "name": "",
                        "type": "uint256"
                    }
                ],
                "payable": false,
                "stateMutability": "view",
                "type": "function"
            },
            {
                "constant": false,
                "inputs": [
                    {
                        "name": "queryId",
                        "type": "bytes32"
                    },
                    {
                        "name": "result",
                        "type": "string"
                    },
                    {
                        "name": "proof",
                        "type": "bytes"
                    }
                ],
                "name": "__callback",
                "outputs": [],
                "payable": false,
                "stateMutability": "nonpayable",
                "type": "function"
            },
            {
                "constant": true,
                "inputs": [
                    {
                        "name": "",
                        "type": "uint256"
                    }
                ],
                "name": "totalAmountsBet",
                "outputs": [
                    {
                        "name": "",
                        "type": "uint256"
                    }
                ],
                "payable": false,
                "stateMutability": "view",
                "type": "function"
            },
            {
                "constant": true,
                "inputs": [],
                "name": "canBet",
                "outputs": [
                    {
                        "name": "",
                        "type": "bool"
                    }
                ],
                "payable": false,
                "stateMutability": "view",
                "type": "function"
            },
            {
                "constant": true,
                "inputs": [],
                "name": "totalBetAmount",
                "outputs": [
                    {
                        "name": "",
                        "type": "uint256"
                    }
                ],
                "payable": false,
                "stateMutability": "view",
                "type": "function"
            },
            {
                "constant": true,
                "inputs": [],
                "name": "TOTAL_AMOUNT_NEEDED",
                "outputs": [
                    {
                        "name": "",
                        "type": "uint256"
                    }
                ],
                "payable": false,
                "stateMutability": "view",
                "type": "function"
            },
            {
                "constant": false,
                "inputs": [
                    {
                        "name": "teamIdx",
                        "type": "uint256"
                    }
                ],
                "name": "bet",
                "outputs": [],
                "payable": true,
                "stateMutability": "payable",
                "type": "function"
            },
            {
                "constant": true,
                "inputs": [],
                "name": "MINIMUM_BET",
                "outputs": [
                    {
                        "name": "",
                        "type": "uint256"
                    }
                ],
                "payable": false,
                "stateMutability": "view",
                "type": "function"
            },
            {
                "constant": false,
                "inputs": [],
                "name": "Superbowl",
                "outputs": [],
                "payable": false,
                "stateMutability": "nonpayable",
                "type": "function"
            },
            {
                "constant": true,
                "inputs": [],
                "name": "payoutCompleted",
                "outputs": [
                    {
                        "name": "",
                        "type": "bool"
                    }
                ],
                "payable": false,
                "stateMutability": "view",
                "type": "function"
            },
            {
                "constant": true,
                "inputs": [],
                "name": "winningTeam",
                "outputs": [
                    {
                        "name": "",
                        "type": "uint8"
                    }
                ],
                "payable": false,
                "stateMutability": "view",
                "type": "function"
            },
            {
                "constant": true,
                "inputs": [],
                "name": "BOOKIE_POOL_COMMISSION",
                "outputs": [
                    {
                        "name": "",
                        "type": "uint256"
                    }
                ],
                "payable": false,
                "stateMutability": "view",
                "type": "function"
            },
            {
                "constant": true,
                "inputs": [
                    {
                        "name": "",
                        "type": "uint256"
                    }
                ],
                "name": "TEAM_NAMES",
                "outputs": [
                    {
                        "name": "",
                        "type": "string"
                    }
                ],
                "payable": false,
                "stateMutability": "view",
                "type": "function"
            },
            {
                "anonymous": false,
                "inputs": [],
                "name": "BetMade",
                "type": "event"
            },
            {
                "anonymous": false,
                "inputs": [],
                "name": "TotalAmountReached",
                "type": "event"
            },
            {
                "anonymous": false,
                "inputs": [
                    {
                        "indexed": false,
                        "name": "_amount",
                        "type": "uint256"
                    }
                ],
                "name": "OwnerGetTenPercent",
                "type": "event"
            },
            {
                "anonymous": false,
                "inputs": [
                    {
                        "indexed": false,
                        "name": "_address",
                        "type": "address"
                    },
                    {
                        "indexed": false,
                        "name": "_amount",
                        "type": "uint256"
                    }
                ],
                "name": "TransferEtherToWinner",
                "type": "event"
            }
        ]);
        betInstance = NumberBetContract.at('0x28E17cFE986b8A98d74BCFeCFf8791e108e54b5A');
        betInstance.totalBetAmount(function(error, total){
            if (!error) {
                if (total > 0) {
                    $('.total-bet-amount').text(web3.fromWei(total, "ether").toFixed(3));
                } else {
                    $('.total-bet-amount').text(0);
                }
                betInstance.TOTAL_AMOUNT_NEEDED(function(error, needed){
                    if (error) {
                        console.log(error);
                    } else {
                        console.log(needed);
                        var remaining = total - needed;
                        if (remaining > 0) {
                            $('.amount-needed').text(web3.fromWei(remaining, "ether").toFixed(3));
                        } else {
                            $('.amount-needed').text(0.000);
                        }
                    }
                });
            } else {
                console.log(error);
            }
        });

        betInstance.winningTeam.call(function(error, result){
            if (!error) {
                var winningTeam = 'None';
                if (result == 0) {
                    winningTeam = 'Heads';
                } else if (result == 1) {
                    winningTeam = 'Tails';
                }
                $('.winning-team').text(winningTeam);
            } else {
                console.log(error);
            }
        });
        betInstance.totalAmountsBet(0, function(error, result){
            if (!error) {
                $('.heads-amount').text(web3.fromWei(result, "ether").toFixed(3));
            } else {
                console.log(error);
            }
        });
        betInstance.totalAmountsBet(1, function(error, result){
            if (!error) {
                $('.tails-amount').text(web3.fromWei(result, "ether").toFixed(3));
            } else {
                console.log(error);
            }
        });
        return App.bindEvents();
    },
    bindEvents: function() {
        $(document).on('click', '.bet-heads', App.betHeads);
        $(document).on('click', '.bet-tails', App.betTails);
    },
    betHeads: function() {
      var amount = parseFloat($('.heads-ether').val());

      return App.startBet(0, amount);
    },
    betTails: function() {
      var amount = parseFloat($('.tails-ether').val());

      return App.startBet(1, amount);
    },
    startBet: function(side, amount) {
        var instance;

        if (typeof amount === "number" && amount > 0) {
          console.log(side);

          web3.eth.getAccounts(function(error, accounts){
              if (!error) {
                  var account = accounts[0];

                  betInstance.bet(side, {from: account,
                      value: web3.toWei(amount, 'ether'),
                      gas: 150000,
                      gasPrice: web3.toWei(4, 'gwei')
                  }, function(error, result){
                      if (!error) {
                          console.log(result);
                          toastr.options = {
                            "closeButton": true,
                            "debug": false,
                            "newestOnTop": false,
                            "progressBar": false,
                            "positionClass": "toast-top-full-width",
                            "preventDuplicates": false,
                            "onclick": null,
                            "showDuration": "300",
                            "hideDuration": "5000",
                            "timeOut": "5000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                          }
                          toastr.success('The transaction should be confirmed by the Etherum network within a minute. Check MetaMask to verify this. Winnings will be paid out automatically by the smart contract.', 'Your bet has been processed.');
                      } else {
                          console.log(error);
                      }
                  });
              } else {
                  console.log(error);
              }
          });

          var BetEvent = betInstance.BetMade();
          BetEvent.watch(function(error, result){
              if (!error) {
                  var event = result.event;
                  if (event == 'BetMade') {
                      console.log("bet maded");
                  }
                  console.log(result);
              } else {
                  console.log(error);
              }
          });
        } else {
          window.alert("ether is not a number");
        }
    }
};

$(function(){
    $(window).load(function() {
        App.initWeb3();
    });
});
